package br.com.trama.jaxrsjpajboss.rest;

import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.trama.jaxrsjpajboss.service.ServiceEJB;

@Path("/hello")
public class HelloRest {
	
	@Inject
	private ServiceEJB ejb;
	
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/text")
    public String helloResource() {
        return "Hello! It's "+new Date();
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/ejb")
    public String runEJB(){
    	ejb.run();
    	return "string";
    }
}
