package br.com.trama.jaxrsjpajboss.model.entity;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public class SettingsDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public SettingsDAO(){}

	public void save(Settings settings){
		
		if(settings.getId() == null){
			entityManager.persist(settings);
		}
		else {
			entityManager.merge(settings);
		}
	}

	public List<Settings> findAll() {

		List<Settings> resultList = null;

		TypedQuery<Settings> query = entityManager.createQuery("FROM Settings e ORDER BY e.id DESC", Settings.class);

		try{
			resultList = query.getResultList();

		}catch(NoResultException e){
			resultList = Collections.emptyList();
		}

		return resultList;	
	}
}
