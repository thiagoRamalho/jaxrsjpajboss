package br.com.trama.jaxrsjpajboss.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import br.com.trama.jaxrsjpajboss.model.entity.Settings;
import br.com.trama.jaxrsjpajboss.model.entity.SettingsDAO;

@Stateless
@PersistenceContext(name="ServiceEJBPersistenceContext", type = PersistenceContextType.EXTENDED)
public class AsyncServiceEJB {

	@Inject
	private SettingsDAO settingsDAO;
	
	@Asynchronous
	public void execute(String string){

		System.out.println("START "+this.getClass().getSimpleName());
		
		System.out.println("PARAMETER MSG: "+string);
		
		List<Settings> findAll = settingsDAO.findAll();
		
		for (Settings settings : findAll) {
			
			System.out.println("PERSISTED: "+settings);
			
			settings.setUrl("change.com/"+new Date());
			
			settingsDAO.save(settings);
		}
		
		findAll = settingsDAO.findAll();
		
		for (Settings settings : findAll) {
			System.out.println("UPDATED: "+settings);
		}
		
		System.out.println("FINISH "+this.getClass().getSimpleName());
	}
}
