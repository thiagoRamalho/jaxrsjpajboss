package br.com.trama.jaxrsjpajboss.service;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import br.com.trama.jaxrsjpajboss.model.entity.Settings;
import br.com.trama.jaxrsjpajboss.model.entity.SettingsDAO;

@Stateless
@PersistenceContext(name="ServiceEJBPersistenceContext", type = PersistenceContextType.EXTENDED)
public class ServiceEJB {

	@EJB
	private AsyncServiceEJB asyncServiceEJB;

	@Inject
	private SettingsDAO settingsDAO;

	public void run(){

		System.out.println("START "+this.getClass().getSimpleName());

		Settings settings = new Settings();

		settings.setUrl("www.url.com/"+System.currentTimeMillis());

		settingsDAO.save(settings);

		asyncServiceEJB.execute("invoke event: "+new Date());
		
		System.out.println("FINISH "+this.getClass().getSimpleName());
	}
}
