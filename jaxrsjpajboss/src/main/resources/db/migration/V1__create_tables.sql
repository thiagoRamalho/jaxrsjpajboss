-- DROP TABLE settings;

CREATE TABLE settings
(
  id serial NOT NULL,
  url character varying(255),
  CONSTRAINT settings_pkey PRIMARY KEY (id)
);

-- DROP TABLE version;
CREATE TABLE version
(
  id serial NOT NULL,
  urldownload character varying(255),
  version bigint NOT NULL,
  CONSTRAINT version_pkey PRIMARY KEY (id)
);

