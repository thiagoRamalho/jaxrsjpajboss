-- shortcut maven
clean compile -e flyway:migrate package install cobertura:cobertura jboss-as:deploy
-- url
http://127.0.0.1:8080/jaxrsjpajboss/rest/hello

-- standalone (verificar se existe o mudules do postgres)
				<datasource jndi-name="java:jboss/datasources/jaxrsjpajbossDS"
					pool-name="jaxrsjpajbossDS" enabled="true" use-java-context="true">
					<connection-url>jdbc:postgresql:jaxrsjpajbossDB</connection-url>
					<driver>org.postgresql</driver>
					<security>
						<user-name>postgres</user-name>
						<password>postgres</password>
					</security>
				</datasource>

--o 'module' deve ser o mesmo nome configurado no arquivo module.xml-->
<driver name="org.postgresql" module="org.postgresql">
    <xa-datasource-class>org.postgresql.Driver</xa-datasource-class>
</driver>
